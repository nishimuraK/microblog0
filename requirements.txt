asgiref==3.4.1
Django
pytz
sqlparse==0.4.2

psycopg2
gunicorn

dj-database-url
whitenoise