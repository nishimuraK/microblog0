# from django.shoutcut import render
from django.db import models
# Create your models here.
class Blog(models.Model):

  #id =1,2,3,4　保存されるたびに保存したものにたいして　idがつく機能がついてる
  content = models.CharField(max_length=140, verbose_name='本文')
  #文字列を保存するので CharField
  posted_date =models.DateTimeField(auto_now_add=True)
  #投稿時一回だけ日時を加えている
  #データを保存する場合は　なになにFieldと最後にいれる


  class Meta:
    ordering = ['-posted_date', ]