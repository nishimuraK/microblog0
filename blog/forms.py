from django import forms
from .models import Blog

class BlogForm(forms.ModelForm):

  content = forms.CharField(widget=forms.TextInput(attrs={"size":50}))
  #CharFieldが文字を保存しますよと言う意味,TextInputがhtmlで言うinputタグにあたる.

  class Meta:
    model = Blog
    fields = ["content",]