from django.http import request
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
# mixinというのはclassに継承させることで、その機能を追加する。
# LoginRequiredMixinをしたで追加させる時,先にLoginRequiredMixinを書いてそのあとにCreateViewやUpdate,を書く、たまに逆にするとエラーが起こる時があるため。
from django.urls import reverse_lazy
#汎用クラスビューなので、
from django.views.generic import CreateView,UpdateView
from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic import DeleteView
from .models import Blog
from .forms import BlogForm


class BlogListView(ListView):
  model = Blog
  paginate_by = 5
  context_object_name = "blogs"

class BlogDetailView(DetailView):
  model = Blog
  # model =BlogでブログというDBからデータを引き出している
  context_object_name = "blog"

class BlogCreateView(LoginRequiredMixin,CreateView):
  model = Blog
  #入力が加わって来るので、modelの中にあるcontentを使う
  form_class = BlogForm
  success_url = reverse_lazy('index')
  template_name = "blog/blog_create_form.html"

  #LoginRequiredMixin これを使う時必ず定義しないといけないのkが　login_url
  login_url = '/login'
  #ログインしていないユーザーがcreateやupdateにurlから飛ぼうとすると、強制的にloginに飛ばすというコード

  #上が成功した場合したが発生する
  def form_valid(self, form):
      messages.success(self.request,'保存しました')
      return super().form_valid(form)

  # #失敗した時のメッセージ
  def form_invalid(self, form):
    messages.error(self.request, "保存に失敗しました")
    return super().form_invalid(form)

    #veiws.pyでは文字をセットしただけなので表示させる場合はテンプレート htmlも設定しましょう

class BlogUpdateView(LoginRequiredMixin, UpdateView):
  model = Blog
  form_class = BlogForm
  template_name = "blog/blog_update_form.html"
  login_url = '/login'

  def get_success_url(self):
    blog_pk = self.kwargs['pk'] #このpkは<int:pk>のpk
    url = reverse_lazy("detail",kwargs={"pk": blog_pk})
    return url

  def form_valid(self, form):
    messages.success(self.request, "更新されました。")
    return super().form_valid(form)

  def form_invalid(self, form):
    messages.error(self.request, "更新されませんでした。")
    return super().form_invalid(form)


class BlogDeleteView(LoginRequiredMixin,DeleteView):
  model = Blog
  success_url = reverse_lazy('index')
  login_url = '/login'

  def delete(self, request, *args , **kwargs):
    messages.success(self.request, '消去しました。')
    return super().delete(request, *args, **kwargs)

