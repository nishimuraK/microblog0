from django.test import TestCase,Client

# Create your tests here.
class BlogTestCase(TestCase):

  def setUp(self):
    self.c = Client()
    # setUpは各テストが実装されるよりも先に実行される関数

  def test_index_access(self):
    res = self.c.get('/')

    # status code ==> 200
    # 200以上なら成功
    # status code ==> 404 Not Found
    # status code ==> 302 Redirect
    # コードの番号によって結果を教えてくれる
    self.assertEqual(200,res.status_code)


  def test_fail_access(self):
    self.assert_(False)
    res = self.c.get('/unknown')
    print(res.status_code)
    self.assertEqual(404,res.status_code)