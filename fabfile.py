from fabric.api import cd, sudo, shell_env, run, env

HOST_PATH = "/opt/django/microblog/microblog0"

ex_env = {
    "DJANGO_SETTINGS_MODULE": "microblog.settings.prod",
}

env.hosts = ["192.168.1.55", ]
env.user = "root"
env.password = "lcver6mdmg"


def collect_static():
    with cd(HOST_PATH):
        with shell_env(**ex_env):
            run("/opt/django/microblog/bin/python manage.py collectstatic --no-input")


def git_update():
    with cd(HOST_PATH):
        run("git pull")


def kill():
    import time
    sudo("pkill -f gunicorn")
    time.sleep(1)
    sudo("ps aux | grep gunicorn")

def start():
    import time
    with cd(HOST_PATH):
        with shell_env(**ex_env):
            sudo("/opt/django/microblog/bin/gunicorn -c gunicorn.conf.py microblog.wsgi")
            time.sleep(1)
            sudo("ps aux | grep gunicorn")

def send_hup_signal():
    import time
    with cd(HOST_PATH):
        with shell_env(**ex_env):
            sudo("kill -HUP `cat /var/run/microblog.pid`")
            time.sleep(1)
            sudo("ps aux | grep gunicorn")


def restart():
    import time
    kill()
    time.sleep(1)
    start()
    send_hup_signal()


def all():
    git_update()
    collect_static()
    restart()